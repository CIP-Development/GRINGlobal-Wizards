## How to install the Wizards for Curator Tool

1. **Copy the DLL files to the folder "C:\Program Files\GRIN-Global\GRIN-Global Curator Tool\Wizards"**
<br><br>![install_step2](https://gitlab.com/CIP-Development/GRINGlobal-Wizards/-/wikis/uploads/b68003fc07cf189eaaa250ece7a8cfcd/install_step1.PNG)
2. **Right click the DLL file and in properties check the UNBLOCK option**
<br/><br>![install_step2](https://gitlab.com/CIP-Development/GRINGlobal-Wizards/-/wikis/uploads/f2e6076bbf8222f338b9526d1c9b0f37/install_step2.PNG)

___
Note 1: DLL binary files, dataviews and installer files in [https://gitlab.com/GRIN-Global](https://gitlab.com/GRIN-Global)

Note 2: Training videos on YouTube [https://www.youtube.com/channel/UCWexOCDil5l7VJP1WrdEjeQ](https://www.youtube.com/channel/UCWexOCDil5l7VJP1WrdEjeQ)

Note 3: Source code for International Wizards in [https://gitlab.com/CIP-Development/grin-global_client_international](https://gitlab.com/CIP-Development/grin-global_client_international) 

Note 4: Source code for Mobile App in [https://gitlab.com/CIP-Development/inventorymobileapp](https://gitlab.com/CIP-Development/inventorymobileapp)

Note 5: Source code for RESTFul Webservices in [https://gitlab.com/CIP-Development/GRIN-Global-WCFService](https://gitlab.com/CIP-Development/GRIN-Global-WCFService) 
